#include <stdio.h>
#include <math.h>

// Parámetros del oscilador armónico cuántico
double omega = 1.0;  // Frecuencia angular
double E = 0.5;      // Energía
int l = 0;           // Número cuántico orbital

// Función que calcula el valor de la función de onda usando el método de Numerov
void numerov(int n, double rmax) {
    double h = rmax / n;
    double r[n+1];
    double u[n+1];
    
    // Inicialización de condiciones iniciales
    r[0] = 0.0;
    u[0] = 0.0;
    r[1] = h;
    u[1] = 1e-6;  // Pequeña perturbación para evitar división por cero

    FILE *file = fopen("funcion_de_onda.txt", "w");
    if (file == NULL) {
        perror("No se pudo abrir el archivo.");
        return;
    }

    for (int i = 1; i < n; i++) {
        r[i+1] = r[i] + h;
        
        // Aplicar el método de Numerov
        double k2 = l * (l + 1) / (r[i] * r[i]) + 0.25 * omega * omega * r[i] * r[i] - E;
        u[i+1] = (2 * (1 - 5 * h * h * k2 / 12) * u[i] - (1 + h * h * k2 / 12) * u[i-1]) / (1 + h * h * k2 / 12);

        // Escribir los datos en el archivo
        fprintf(file, "%f %f\n", r[i], u[i]);
    }

    fclose(file);
}

int main() {
    int n = 1000;     // Número de puntos de la malla
    double rmax = 10; // Valor máximo de r

    numerov(n, rmax);

    return 0;
}
